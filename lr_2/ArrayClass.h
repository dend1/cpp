//
// Created by denis on 31.03.2019.
//

#ifndef UNTITLED_ARRAYCLASS_H
#define UNTITLED_ARRAYCLASS_H

#include <iostream>
using namespace std;


template <typename TYPE>
class ArrayClass{
private:
    TYPE *_array_;
    int _size_;
public:
    ArrayClass();
    ArrayClass(int _size_);
    ArrayClass(int _size_, TYPE _value_);
    ArrayClass(ArrayClass<TYPE> &_array_);
    TYPE &operator[](int _index_);
    ArrayClass<TYPE> operator=(ArrayClass<TYPE> &_array_);
    void copyMethod(ArrayClass<TYPE> &_array_);
    TYPE cutOut(int item);
    const int size();
    ~ArrayClass();
};


template<typename TYPE>
ArrayClass<TYPE>::ArrayClass() : _array_(NULL), _size_(0) {}

template<typename TYPE>
ArrayClass<TYPE>::ArrayClass(int _size_) : _size_(_size_) {
    _array_ = new TYPE[_size_];
}

template<typename TYPE>
ArrayClass<TYPE>::ArrayClass(int _size_, TYPE _value_) : ArrayClass(_size_) {
    for (int i = 0; i < _size_; ++i) {
        _array_[i] = _value_;
    }
}

template<typename TYPE>
ArrayClass<TYPE>::ArrayClass(ArrayClass<TYPE> &_array_) {
    copyMethod(_array_);
}

template<typename TYPE>
TYPE &ArrayClass<TYPE>::operator[](int _index_) {
    return _array_[_index_];
}

template<typename TYPE>
ArrayClass<TYPE> ArrayClass<TYPE>::operator=(ArrayClass<TYPE> &_array_) {
    if (this -> _array_ != _array_._array_) {
        this -> ~ArrayClass();
        copyMethod(_array_);
    }
    return *this;
}

template<typename TYPE>
void ArrayClass<TYPE>::copyMethod(ArrayClass<TYPE> &_array_) {
    _size_ = _array_._size_;
    this -> _array_ = new TYPE[_size_];
    for (int i = 0; i + 1 < _size_; ++i) {
        this ->_array_[i] = _array_[i];
    }
}

template<typename TYPE>
TYPE ArrayClass<TYPE>::cutOut(int item) {
    TYPE value = _array_[item];
    for (int i = item; i < _size_; ++i) {
        _array_[i] = _array_[i + 1];
    }
    _size_--;
    return value;
}

template<typename TYPE>
const int ArrayClass<TYPE>::size() {
    return _size_;
}

template<typename TYPE>
ArrayClass<TYPE>::~ArrayClass() {
    delete _array_;
}


#endif //UNTITLED_ARRAYCLASS_H
