#include <iostream>
#include "ArrayClass.h"
using namespace std;


int main() {
    ArrayClass<int> arr(5, 10);
    cout << arr.size() << endl;
    cout << arr.cutOut(0) << endl;
    cout << arr.size() << endl;
    return 0;
}