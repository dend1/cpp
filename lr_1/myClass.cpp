#include "myClass.h"
#include <iostream>
#include <string>
#include <cmath>
using namespace std;

double myClass::operator()(double x){
    this->x = x;
    if (x <= 0) {
        return exp((pow((c - x)/a, d)));
    }
    else {
        return exp((pow((x - c)/b, d)));
    }
}

double myClass::inverse(){
    if (x <= 0) {
        if ((c - x)/a >= 0) {
            return log((c - x)/a);
        }
        else {
            return 0;
        }
    }
    else {
        if ((x - c)/b >= 0) {
            return log((x - c)/b);
        }
        else {
            return 0;
        }
    }
}

string myClass::show()
{
    string function;
    if (x <=0){
        function = "e^((("+ to_string(c) + "-" + to_string(x) + ")/" + to_string(a) + ")^" + to_string(d) + ")";
    } else {
        function = "e^((("+ to_string(x) + "-" + to_string(c) + ")/" + to_string(b) + ")^" + to_string(d) + ")";
    }
    return function;
}

double myClass::get_a(){
    return a;
};

void myClass::set_a(double a){
    this->a = a;
};

double myClass::get_b(){
    return b;
};

void myClass::set_b(double b){
    this->b = b;
};

double myClass::get_c(){
    return c;
};

void myClass::set_c(double c){
    this->c = c;
};

double myClass::get_d(){
    return d;
};

void myClass::set_d(double d){
    this->d = d;
};

double myClass::get_x(){
    return x;
};

void myClass::set_x(double x){
    this->x = x;
};

