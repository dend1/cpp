#include <iostream>
#include <string>
#include "myClass.h"

using namespace std;



int main() {
    double a = 6, b = 4, c = 10, d = 1, x = 5;
    myClass func(a, b, c, d);
    cout << func(x) << endl;
    cout << func.show() << endl;
    cout << "a: " << func.get_a() << endl;
    func.set_a(10);
    cout << "new a: " << func.get_a() << endl;
    return 0;
}