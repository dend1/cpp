#include <iostream>
#include <string>
using namespace std;

class myClass {
private:
    double a, b, c, d, x;
public:
    myClass(double a, double b, double c, double d): a(a), b(b), c(c), d(d), x(NULL){};

    myClass(double a, double b, double c): myClass(a, b, c, NULL){};

    myClass(double a, double b): myClass(a, b, NULL){};

    explicit myClass(double a): myClass(a, NULL){};

    myClass(): myClass(NULL){};

    double operator()(double x);

    string show();

    double get_a();

    void set_a(double a);

    double get_b();

    void set_b(double b);

    double get_c();

    void set_c(double c);

    double get_d();

    void set_d(double d);

    double get_x();

    void set_x(double x);

    double inverse();

};
